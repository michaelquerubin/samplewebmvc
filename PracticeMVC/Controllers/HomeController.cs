﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PracticeMVC.Models;

namespace PracticeMVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Dashboard";
            ViewData["ContentTitle"] = "Home Page";

            return View();
        }

        public IActionResult Dashboard()
        {
            ViewData["Title"] = "Dashboard";
            ViewData["ContentTitle"] = "Home Page";

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Route("Logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            return RedirectToAction("Index", "Login", new { area = "" });
        }
    }
}

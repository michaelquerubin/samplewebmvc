﻿var UserID = $.session.get("UserID");
var custom_date_formats = {
    past: [
        { ceiling: 60, text: "$seconds seconds ago" },
        { ceiling: 3600, text: "$minutes minutes ago" },
        { ceiling: 86400, text: "$hours hours ago" },
        { ceiling: 2629744, text: "$days days ago" },
        { ceiling: 31556926, text: "$months months ago" },
        { ceiling: null, text: "$years years ago" }
    ],
    future: [
        { ceiling: 60, text: "in $seconds seconds ago" },
        { ceiling: 3600, text: "in $minutes minutes ago" },
        { ceiling: 86400, text: "in $hours hours ago" },
        { ceiling: 2629744, text: "in $days days ago" },
        { ceiling: 31556926, text: "in $months months ago" },
        { ceiling: null, text: "in $years years ago" }
    ]
};


$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "http://localhost:55864/api/GetUserPost",
        success: function (response) {
            var GetPostUserList = response.Data.PostUserList;
            $.each($.parseJSON(GetPostUserList), function (idx, obj) {
                var PostDate = new Date(obj.PostDate);
                var GetTimeSince = humanized_time_span(Date.now(), PostDate, custom_date_formats);
                if (UserID == obj.UserID) {
                    $('#PostUserList').append('<div class="col-12 animated fadeIn"><div class="card single-blog-post"><div class="img-holder"><div class="img-post g-bg-soundcloud py-5"></div><div class="date-box"><span>' + PostDate.getDate() + '</span><br>' + PostDate.toLocaleString('en-us', { month: 'long' }) + '</div></div> <div class="body"><ul class="meta list-inline"><li><a href="#"><i class="zmdi zmdi-account col-blue"></i>Posted By: ' + obj.Fullname + '</a></li></ul><pre class="">' + obj.Post + '</pre> <div class="row"><div class="col-6"><label>' + GetTimeSince + '</label></div> <div class="col-6 text-right"><a href="#" class="my-3" data-toggle="collapse" data-target="#collapse_' + obj.PostID + '" aria-expanded="false" aria-controls="collapse_' + obj.PostID + '"><i class="material-icons">keyboard_arrow_down</i></a></div></div> </div> </div> <div class="collapse" id="collapse_' + obj.PostID + '" aria-expanded="false" style=""><div class="card single-blog-post text-right"><div class="btn-group text-right my-3 mx-2" role="group" aria-label="Button group"><button type="button" class="btn  btn-raised btn-sm bg-blue waves-effect" onclick="EditMessageContent(this,' + obj.PostID + ')">EDIT</button> <button type="button" class="btn  btn-raised btn-sm bg-red waves-effect" onclick="DeleteMessageContent(this,' + obj.PostID + ')">DELETE</button> </div></div> </div>');
                }
                else {
                    $('#PostUserList').append('<div class="col-12 animated fadeIn"><div class="card single-blog-post"><div class="img-holder"><div class="img-post g-bg-soundcloud py-5"></div><div class="date-box"><span>' + PostDate.getDate() + '</span><br>' + PostDate.toLocaleString('en-us', { month: 'long' }) + '</div></div> <div class="body"><ul class="meta list-inline"><li><a href="#"><i class="zmdi zmdi-account col-blue"></i>Posted By: ' + obj.Fullname + '</a></li></ul><pre class="">' + obj.Post + '</pre> <div class="row"><div class="col-6"><label>' + GetTimeSince + '</label></div>  </div> </div> </div>');
                }
                
            });

            localStorage.setItem("PostUserList", GetPostUserList);

            //$.session.set("PostUserList", GetPostUserList);
        },
        error: function (jqXHR, exception) {
            $.toast({
                heading: 'Error',
                text: 'Web API is not available right now',
                icon: 'error',
                position: 'top-right',
                hideAfter: 5000,
                loader: true,        // Change it to false to disable loader
                loaderBg: '#9EC600'  // To change the background
            });
        }
    });
});

setInterval(function () {
    var OrigPostUserList = $.parseJSON(localStorage.getItem("PostUserList"));
    $.ajax({
        type: "GET",
        url: "http://localhost:55864/api/GetUserPost",
        success: function (response) {
            var GetPostUserList = $.parseJSON(response.Data.PostUserList);
            var GetPostUserListID = GetPostUserList[0].PostID;
            if (GetPostUserList.length != OrigPostUserList.length) {
                $.each(GetPostUserList, function (i, obj) {
                    if (obj.PostID == GetPostUserListID) {
                        var PostDate = new Date(obj.PostDate);
                        var GetTimeSince = humanized_time_span(Date.now(), PostDate, custom_date_formats);
                        if (UserID == obj.UserID) {
                            $('#PostUserList').prepend('<div class="col-12 animated fadeIn"><div class="card single-blog-post"><div class="img-holder"><div class="img-post g-bg-soundcloud py-5"></div><div class="date-box"><span>' + PostDate.getDate() + '</span><br>' + PostDate.toLocaleString('en-us', { month: 'long' }) + '</div></div> <div class="body"><ul class="meta list-inline"><li><a href="#"><i class="zmdi zmdi-account col-blue"></i>Posted By: ' + obj.Fullname + '</a></li></ul><pre class="">' + obj.Post + '</pre> <div class="row"><div class="col-6"><label>' + GetTimeSince + '</label></div> <div class="col-6 text-right"><a href="#" class="my-3" data-toggle="collapse" data-target="#collapse_' + obj.PostID + '" aria-expanded="false" aria-controls="collapse_' + obj.PostID + '"><i class="material-icons">keyboard_arrow_down</i></a></div></div> </div> </div> <div class="collapse" id="collapse_' + obj.PostID + '" aria-expanded="false" style=""><div class="card single-blog-post text-right"><div class="btn-group text-right my-3 mx-2" role="group" aria-label="Button group"><button type="button" class="btn  btn-raised btn-sm bg-blue waves-effect" onclick="EditMessageContent(this,' + obj.PostID + ')">EDIT</button> <button type="button" class="btn  btn-raised btn-sm bg-red waves-effect" onclick="DeleteMessageContent(this,' + obj.PostID + ')">DELETE</button> </div></div> </div>');
                        }
                        else {
                            $('#PostUserList').prepend('<div class="col-12 animated fadeIn"><div class="card single-blog-post"><div class="img-holder"><div class="img-post g-bg-soundcloud py-5"></div><div class="date-box"><span>' + PostDate.getDate() + '</span><br>' + PostDate.toLocaleString('en-us', { month: 'long' }) + '</div></div> <div class="body"><ul class="meta list-inline"><li><a href="#"><i class="zmdi zmdi-account col-blue"></i>Posted By: ' + obj.Fullname + '</a></li></ul><pre class="">' + obj.Post + '</pre> <div class="row"><div class="col-6"><label>' + GetTimeSince + '</label></div>  </div> </div> </div>');
                        }
                        
                    }
                });
                localStorage.removeItem("PostUserList");
                localStorage.setItem("PostUserList", response.Data.PostUserList);
                //$.session.set("PostUserList", response.Data.PostUserList);
            }
        }
    });
}, 5000);

$(function () {
    //Textare auto growth
    autosize($('textarea.auto-growth'));
});

var PostMessageContent = function () {
    var txtPost = $("#txtPost").val();
    var UserID = $.session.get("UserID");
    var Results = false;

    if (txtPost === "") {
        $.toast({
            heading: 'Message',
            text: 'Dont be shy to post something...',
            icon: 'error',
            position: 'top-right',
            hideAfter: 5000,
            loader: true,        // Change it to false to disable loader
            loaderBg: '#9EC600'  // To change the background
        });

        //$(".divCimno").addClass("animated fadeIn bg-black");
        //$(".cimno").addClass("error");

        Results = false;
    }
    else {
        //$(".divCimno").removeClass("bg-black");
        //$(".divPassword").removeClass("bg-black");

        //$(".cimno").removeClass("error");
        //$(".password").removeClass("error");

        Results = true;
    }



    if (Results === true) {
        $.ajax({
            type: "GET",
            url: "http://localhost:55864/api/InsertPost",
            data: { "UserID": UserID, "Post": txtPost },
            success: function (response) {
                if (response.Data.UserInfoResult === false) {
                    $.toast({
                        heading: 'Error',
                        text: 'Web API is not available right now',
                        icon: 'error',
                        position: 'top-right',
                        hideAfter: 5000,
                        loader: true,        // Change it to false to disable loader
                        loaderBg: '#9EC600'  // To change the background
                    });
                } else { $("#txtPost").fadeIn(3000).val(""); }

                
            },
            error: function (jqXHR, exception) {
                $.toast({
                    heading: 'Error',
                    text: 'Web API is not available right now',
                    icon: 'error',
                    position: 'top-right',
                    hideAfter: 5000,
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600'  // To change the background
                });
            }
        });
    }
};
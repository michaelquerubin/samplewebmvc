﻿$(document).ready(function () {

});

var ShowPassword = function () {
    if ($("#txtpassword").is('input:password')) {
        $("#txtpassword").prop('type', 'text');
    } else { $("#txtpassword").prop('type', 'password'); }
};

var LoginAccount = function () {
    var CimNo = $("#txtcimno").val();
    var Password = $("#txtpassword").val();
    var Results = false;

    if (CimNo === "" || CimNo === null) {
        $.toast({
            heading: 'Message',
            text: 'Please Enter CIM NO#',
            icon: 'error',
            position: 'top-right',
            hideAfter: 5000,
            loader: true,        // Change it to false to disable loader
            loaderBg: '#9EC600'  // To change the background
        });

        $(".divCimno").addClass("animated fadeIn bg-black");
        $(".cimno").addClass("error");

        Results = false;
    }
    else if (Password === "" || Password === null) {
        $.toast({
            heading: 'Message',
            text: 'Please Enter Password',
            icon: 'error',
            position: 'top-right',
            hideAfter: 5000,
            loader: true,        // Change it to false to disable loader
            loaderBg: '#9EC600'  // To change the background
        });

        $(".divPassword").addClass("animated fadeIn bg-black");
        $(".password").addClass("error");

        Results = false;
    }
    else {
        $(".divCimno").removeClass("bg-black");
        $(".divPassword").removeClass("bg-black");

        $(".cimno").removeClass("error");
        $(".password").removeClass("error");

        Results = true;
    }



    if (Results === true) {
        var apiBaseUrl = "http://localhost:55864/api/";
        var UserName = "";


        $.ajax({
            type: "GET",
            url: apiBaseUrl + "GetUserInformation",
            data: { "unum": CimNo, "upass": Password },
            success: function (response) {
                $.session.set("UserID", response.Data.UserID);
                $.session.set("UserName", response.Data.UserName);
                $.session.set("UserCIM", response.Data.UserCIM);
                $.session.set("UserEmail", response.Data.UserEmail);
                $.session.set("UserAddress", response.Data.UserAddress);
                if (response.Data.UserInfoResult === false) {
                    $.toast({
                        heading: 'Message',
                        text: 'Invalid CIM No or Password',
                        icon: 'error',
                        position: 'top-right',
                        hideAfter: 5000,
                        loader: true,        // Change it to false to disable loader
                        loaderBg: '#9EC600'  // To change the background
                    });
                } else {
                    window.location.href = "/Home/Dashboard";
                }
            },
            error: function (jqXHR, exception) {
                $.toast({
                    heading: 'Error',
                    text: 'Web API is not available right now',
                    icon: 'error',
                    position: 'top-right',
                    hideAfter: 5000,
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600'  // To change the background
                });
            }
        });
    }
};

var ValidateLoginValue = function (GetValue, Param) {
    var ThisValue = GetValue.value;

    if (Param === 1) {
        if (ThisValue === "" || ThisValue === null) {
            $(".divCimno").addClass("bg-black animated fadeIn");
            $(".cimno").addClass("error");
        } else {
            $(".divCimno").removeClass("bg-black animated fadeIn");
            $(".cimno").removeClass("error");
        }
    }
    else if (Param === 2) {
        if (ThisValue === "" || ThisValue === null) {
            $(".divPassword").addClass("bg-black animated fadeIn");
            $(".password").addClass("error");
        }
        else {
            $(".divPassword").removeClass("bg-black animated fadeIn");
            $(".password").removeClass("error");
        }
    }
};
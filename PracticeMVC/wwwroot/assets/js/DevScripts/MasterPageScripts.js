﻿$(document).ready(function () {

    var Username = $.session.get("UserName");
    var UserEmail = $.session.get("UserEmail");

    $("#UserName").text(Username);
    $("#UserEmail").text(UserEmail);

    $.toast({
        heading: 'Welcome',
        text: 'Hi, ' + Username,
        icon: 'success',
        position: 'top-right',
        hideAfter: 5000,
        loader: true,        // Change it to false to disable loader
        loaderBg: '#9EC600'  // To change the background
    });
});